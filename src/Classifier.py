import re
from collections import Counter
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import time
result_arr = []


# test_review is each row of the test_matrix
def knn(distance_list, rating_arr, k):
    collected_distances = []
    # get the cosine similarity of each review to each of the review in train_set
    i = 0
    for item in distance_list:
        collected_distances.append((rating_arr[i], item))
        i += 1
    collected_distances = sorted(collected_distances, key=distance, reverse=True)
    limited_dist_list = collected_distances[0:k]
    res = Counter(elem[0] for elem in limited_dist_list)
    return res.most_common(1)[0][0]


def distance(element):
    return element[1]


start_time = time.time()
train_file = open("train/train.data", "r")
train_corpus = []
rating_labels = []
for review in train_file:
    text_line = review.split("\t")
    # review
    review = text_line[1]
    # rid of all the numbers and special symbols
    new_review = re.sub("[^a-zA-Z_' -]", "", review)
    # for empty reviews
    if new_review != "":
        rating_labels.append(text_line[0])
        train_corpus.append(new_review)
train_file.close()

tfidf_vectorizer = TfidfVectorizer(stop_words='english')
# Learn vocabulary and idf and return term document matrix
train_matrix = tfidf_vectorizer.fit_transform(train_corpus)

# parse the test data
test_file = open("test/test.data", "r")
test_corpus = []
empty_review_index = []
count = 0
for review in test_file:
    new_review = re.sub("[^a-zA-Z_' -]", "", review)
    # for empty reviews
    if new_review != "":
        test_corpus.append(new_review)
    else:
        empty_review_index.append(count)
    count += 1
test_file.close()

# Now transform the test data into a term document matrix using the learned vocabulary and idf from the train_data
test_matrix = tfidf_vectorizer.transform(test_corpus)
count = 0
predicted_num = []
for test in test_matrix:
    count += 1
    similarity_list = cosine_similarity(test, train_matrix).tolist()
    result = knn(similarity_list[0], rating_labels, 130)
    print("Predicted " + str(count) + ":" + result)
    predicted_num.append(result)

for index in range(len(empty_review_index)):
    predicted_num.insert(empty_review_index[index], "-1")
result_file = open("result.txt", "w")
for nums in predicted_num:
    result_file.write(str(nums))
    result_file.write('\n')
result_file.close()
print("--------%s minutes---------" % ((time.time() - start_time)//60))





